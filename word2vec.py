import numpy as np
from tqdm import tqdm
from os.path import isdir
from os import mkdir
import mmap

url = 'http://nlp.stanford.edu/data/glove.6B.zip'

if isdir('glove') is False:
    mkdir('glove')
    print('run \'curl -O ' + url + '\' to download vectors.')


glove_path = "glove/glove.6B.300d.txt"


embedding_vector_length = 300
embeddings_index = {}
word_index = {}


def get_num_lines(file_path):
    fp = open(file_path, "r+")
    buf = mmap.mmap(fp.fileno(), 0)
    lines = 0
    while buf.readline():
        lines += 1
    return lines


# wi = 1
with open(glove_path, "r", encoding="utf8") as f:
    for line in tqdm(f, total=get_num_lines(glove_path)):
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
        # word_index[word] = wi
        # wi += 1


print('Found %s word vectors.' % len(embeddings_index))


def seq2vec(obj_vectors, weighted_mean=False):
    arr_wordvec = []
    for vec in obj_vectors:
        if vec is not None:
            arr_wordvec.append(vec)
    wordvec = np.asarray(arr_wordvec)
    seq2vec = wordvec.mean(axis=0)
    if weighted_mean is True:
        seq2vec *= wordvec.std(axis=0)
    return seq2vec


def get_embedding_matrix(word_index):
    embedding_matrix = np.zeros((len(word_index)+1, embedding_vector_length))
    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = embedding_vector
    return embedding_matrix


def len_embedding_vector():
    return embedding_vector_length
