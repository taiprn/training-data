from xml.etree.ElementTree import parse
from os.path import isdir, join
from os import listdir
import numpy as np
from word2vec import text_to_vec, seq2vec


class Loggable(type):
    @staticmethod
    def wrap(load):
        """Return a wrapped instance method"""

        def outer(self, path, verdict):
            return_value = load(self, path, verdict)
            print(return_value)
            return return_value
        return outer

    def __new__(cls, name, bases, attrs):
        print('new')
        """If the class has a 'load' method, wrap it"""
        if 'load' in attrs:
            attrs['load'] = cls.wrap(attrs['load'])
            print('wrapped')
        return super(Loggable, cls).__new__(cls, name, bases, attrs)


class Entry:
    def __init__(self, **kwargs):
        self.text = kwargs['TEXT'] if 'TEXT' in kwargs else None
        self.title = kwargs['TITLE'] if 'TITLE' in kwargs else None
        self.time = kwargs['DATE'] if 'DATE' in kwargs else None
        self.info = kwargs['INFO'] if 'INFO' in kwargs else None
        self.verdict = kwargs['VERDICT'] if 'VERDICT' in kwargs else None


class Dataset(metaclass=Loggable):
    def __init__(self, **kwargs):
        self.entries = []
        self.verdict = -1
        for key in kwargs:
            t = kwargs[key]
            path = t[0]
            verdict = t[1]
            print(path, verdict)
            self.load(path=path, verdict=verdict)
        # if 'verdict' in kwargs:
        #     self.verdict = kwargs['verdict']
        # if 'path' in kwargs:
        #     self.load(kwargs['path'])

    def load(self, path, verdict):
        if isdir(path):
            oldlen = len(self.entries)
            for file in listdir(path):
                self.load(join(path, file), verdict)
            return ('OK!', 'Done loading', path + ',' +
                    str(len(self.entries) - oldlen), 'new entries')
        try:
            tree = parse(path)
        except Exception as e:
            print(e)
            return (-1, 'Error loading' + path)
        root = tree.getroot()
        new_entries = []
        for elem in tree.iter():
            #print(elem.tag, elem.text)
            if elem.tag == "TEXT":
                new_entries.append(Entry(**{**{"TEXT": elem.text}, **{'VERDICT': verdict}}))
        # new_entries = [Entry(**{"TEXT": child.text, 'VERDICT': verdict}) for child in root]
        #new_entries = [Entry(**{**{"TEXT": child.text for child in writing}, **{'VERDICT': verdict}}) for writing in root]
        for entry in new_entries:
            # print(entry.text)
            entry.vector = text_to_vec(entry.text)
            entry.seq2vec = 0  # seq2vec(entry.vector)
            #print(entry.vector)
			
        self.entries += new_entries

        return ('OK!', 'Done loading',
                path, ',' + str(len(new_entries)), 'new entries')

    def store(self, path):
        inputs = []
        verdicts = []
        for entry in self.entries:
            inputs.append(entry.vector)
            verdicts.append(entry.verdict)
        np.savez(path, input=inputs, verdict=verdicts)


def main():
    d1 = Dataset(
        neg=('C:\\Users\\casofiei\\Documents\\Facultate\\reddit-training-ready-to-share\\negative_examples_anonymous', 0),
        pos=('C:\\Users\\casofiei\\Documents\\Facultate\\reddit-training-ready-to-share\\positive_examples_anonymous', 1))
    d1.store("vecs.npz")
    #    d2 = Dataset(
    #        path='D:\\TrainingData\\positive_examples_anonymous', verdict=1)
    # print(str(Dataset()))


main()
