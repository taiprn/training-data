import rule
# import rule_result


rules = []


def get_entry(rule_res, entry):
    return entry


def process_entity(entry):
    for r in rules:
        rule_res = rule.evaluate(entry)
        entry = get_entry(rule_res, entry)
    return entry
