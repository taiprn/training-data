from enum import Enum
import logical_rule
import transform_rule


class RuleType(Enum):
    LOGICAL = 1
    TRANSFORM = 2


rule_type = RuleType.LOGICAL


def evaluate(dataset_entry):
    if rule_type == RuleType.LOGICAL:
        return logical_rule.evaluate(dataset_entry)
    if rule_type == RuleType.TRANSFORM:
        return transform_rule.evaluate(dataset_entry)
