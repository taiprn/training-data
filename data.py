from xml.etree.ElementTree import parse
import numpy as np
from word2vec import embeddings_index, embedding_vector_length
from os.path import splitext
import re
from matplotlib import pyplot as plt
from tqdm import tqdm
from os.path import isdir, join
from os import mkdir, walk, listdir
import json


dataset_path = "datasets/eRisk 2018 - training/2017 train/"
path_test = "datasets/eRisk 2018 - training/2017 test/"
path_pos = join(dataset_path, "positive_examples_anonymous_chunks")
path_neg = join(dataset_path, "negative_examples_anonymous_chunks")
gt_path = join(dataset_path, "risk_golden_truth.txt")
test_gt_path = join(path_test, "test_golden_truth.txt")


class Dataset:
    def __init__(self, **kwargs):
        with open(gt_path, "r") as f:
            lines = f.readlines()
        self.subjects_verdict = dict()
        for l in lines:
            v = l.rstrip().split()
            self.subjects_verdict[v[0]] = v[1]
        with open(test_gt_path, "r") as f:
            lines = f.readlines()
        self.subjects_verdict_test = dict()
        for l in lines:
            v = l.rstrip().split()
            if len(v) == 2:
                self.subjects_verdict_test[v[0]] = v[1]
        self.word_index = dict()

    def store_dataset(self):
        #self.store(path_pos)
        #self.store(path_neg)
        self.store(path_test, 'test_data', True)

    def store(self, path, folder='train_data', test=False):
        if isdir(folder) is False:
            mkdir(folder)
        for path_root, dirs, files in walk(path):
            for file in tqdm(files):
                _, ext = splitext(file)
                if ext == '.xml':
                    filename = join(path_root, file)
                    subj = file.split("_")
                    chunk = subj[2].split('.')[0]
                    subj = subj[0] + '_' + subj[1]
                    if test:
                        verdict = self.subjects_verdict_test[subj]
                    else:
                        verdict = self.subjects_verdict[subj]
                    try:
                        tree = parse(filename)
                    except Exception as e:
                        print(filename)
                        print(e)
                        continue
                    # root = tree.getroot()
                    text = ""
                    for elem in tree.iter():
                        if elem.tag == "TEXT":
                            text += elem.text + '\n'

                    fname = "_".join([subj, chunk, verdict])
                    fname += '.txt'

                    path = join(folder, fname)
                    with open(path, "wb") as f:
                        f.write(text.encode('utf-8'))

    def words_to_vecs(self, words, max_size, discard=False):
        vecs = np.asarray([self.word_index[w] for w in words if w in self.word_index])
        if discard is True:
            if vecs.shape[0] > max_size:
                return None
        vecs = vecs[:max_size]
        vec = np.zeros(max_size)
        vec[:vecs.shape[0]] = vecs
        return vec

    def dataset_vectors(self):
        path = 'train_data'
        path_vec = 'vectors500'
        if isdir(path_vec) is False:
            mkdir(path_vec)
        lengths = []

        print("Build word index.")
        wi = 1
        for file in tqdm(listdir(path)):
            filename = join(path, file)
            exp = re.compile("train_subject\d+_\d+_(0|1)\.txt")
            if exp.match(file):
                with open(filename, 'rb') as doc:
                    text = str(doc.read())
                    tokens = re.sub("[^\w]", " ", text).split()
                    for tok in tokens:
                        if tok not in self.word_index:
                            self.word_index[tok] = wi
                            wi += 1
                    lengths.append(len(tokens))


        print("Save dataset vectors.")

        for file in tqdm(listdir(path)):
            filename = join(path, file)
            exp = re.compile("train_subject\d+_\d+_(0|1)\.txt")
            if exp.match(file):
                verd = int(file[-5])
                with open(filename, 'rb') as doc:
                    text = str(doc.read())
                    tokens = re.sub("[^\w]", " ", text).split()
                    vecs = self.words_to_vecs(tokens, 500, discard=False)
                    if vecs is not None:
                        fn = join(path_vec, file)
                        np.savez(fn, input=vecs, verdict=verd)


    def store_embedding_matrix(self):
        embedding_matrix = np.zeros((len(self.word_index)+1, embedding_vector_length))
        for word, i in self.word_index.items():
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                embedding_matrix[i] = embedding_vector
        np.save("embedding_matrix.npy", embedding_matrix)

    def store_word_index(self):
        with open("word_index.json", "w") as f:
            json.dump(self.word_index, f)

    # plt.xlim([min(lengths), max(lengths)])
    # plt.hist(lengths, bins=len(set(lengths)))
    # plt.title('lengths')
    # plt.xlabel('variable X')
    # plt.ylabel('count')
    # plt.show()


def main():
    d1 = Dataset()
    d1.store_dataset()
    d1.dataset_vectors()
    #d1.store_embedding_matrix()
    #d1.store_word_index()


main()
