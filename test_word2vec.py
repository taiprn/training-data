import unittest
from word2vec import *


class Word2VecTest(unittest.TestCase):
    def testText2Vec(self):
        text = "Yeah at customs. We're talking about when you deliver it to Simeon."
        vec = text_to_vec(text)
        self.assertTrue(len(vec) == len(text.split()))

    def testSeq2Vec(self):
        text = "Yeah at customs. We're talking about when you deliver it to Simeon."
        vec = text_to_vec(text)
        seqvec = seq2vec(vec)
        vec_len = len_embedding_vector()
        self.assertTrue(len(seqvec.shape) == 1)
        self.assertTrue(seqvec.shape[0] == vec_len)


if __name__ == '__main__':
    unittest.main()
